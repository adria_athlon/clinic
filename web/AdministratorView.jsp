<%-- 
    Document   : AdministratorView
    Created on : Jan 16, 2020, 10:48:43 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="AdministratorView.jsp">Administrator Main Menu</a> 
    
    </ul>
    <div class="navbar-nav navbar-right">
        <a href="./index.jsp">Logout</a>
    </div>
  </nav>
<div class="container">
    <div class="row align-items-start">
    <div class="col-6">
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Create New Account</h5>
        <p class="card-text">Create new staff account.</p>
        <a href="./administrator/CreateAccount.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Terminate Account</h5>
        <p class="card-text">Remove staff account.</p>
        <a href="./administrator/RemoveUser.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Ratings Review</h5>
        <p class="card-text">Review doctor ratings.</p>
        <a href="./administrator/ViewDoctorRatings.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
      <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Ratings Comment</h5>
        <p class="card-text">Comment against doctor ratings.</p>
        <a href="./administrator/CommentDoctors.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    
</div>
    
</body>
</html>

