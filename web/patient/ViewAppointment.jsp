<%-- 
    Document   : ViewAppointment
    Created on : Jan 16, 2020, 10:59:17 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<title>View Appointments</title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="PatientView.jsp">Patient Main Menu</a> 
    
    </ul>
    
  </nav>
<div class="container">
	<form name="appointment_from" action="../PatientViewAppointmentServlet" method="post">
		<table align="center">
			<tr>
				<td align="center" colspan="4">
					<h3>View scheduled appointment</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td  width="160px">Patient's Unique ID：</td>
				<td><input type="text" name="id"/></td>
			</tr>
			<tr align="center" bgcolor="#e1ffc1">
				<td><b>patient ID</b></td>
				<td><b>doctor ID</b></td>
				<td><b>start Time</b></td>
				<td><b>end Time</b></td>
			</tr>
			<c:forEach items="${requestScope.list}" var="appointment">
				<tr align="center" bgcolor="white">
					<td>${appointment.getPatientid()}</td>
					<td>${appointment.getDoctorid()}</td>
					<td>${appointment.getStartTime()}</td>
					<td>${appointment.getEndTime()}</td>
				</tr>
			</c:forEach>
			<tr>
				<td align="center" colspan="2"><input type="submit" class="btn btn-info" value="View"></td>
			</tr>
		</table>
	</form>
</body>
</html>

