<%-- 
    Document   : CreateAccount
    Created on : Jan 16, 2020, 10:57:59 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


<title>Create Account</title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="PatientView.jsp">Create new account</a> 
    
    </ul>
    
  </nav>
<div class="container">
   <form name="form2" action="../PatientCreateAccount" method="post" >
		<table align="center" width="400" cellspacing="5px">
			<tr align="center">
				<td align="center" colspan="2">
					<h3>Create new account</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td  width="160px">Unique ID：</td>
				<td><input type="text" name="id"/></td>
			</tr>			
			<tr>
				<td  width="160px">Password：</td>
				<td><input type="text" name="pwd"/></td>
			</tr>
			<tr>
				<td  width="160px">Age：</td>
				<td><input type="text" name="age"/></td>
			</tr>			
			<tr>
				<td width="160px">User：</td>
				<td colspan="2"><select name="gender" size="1">
						
						<option>Male</option>
						<option>Female</option>
						
				</select></td>
			</tr>
                        <tr>
				<td  width="160px">Address：</td>
				<td><input type="text" name="address"/></td>
			</tr>			
			<tr>
				<td align="center" colspan="2">
					<input type="submit" class="btn btn-info" value="Create">
				</td>
			</tr>
	     </table>
    </form>
</body>
</html>

