<%-- 
    Document   : ViewDoctorsRatings
    Created on : Jan 16, 2020, 10:59:43 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<title>Doctor Ratings</title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="PatientView.jsp">Patient Main Menu</a> 
    
    </ul>
    
  </nav>
<div class="container">
	<form name="Set_Doctor_Ratings_Form"
		action="../PatientSetDoctorsRatings" method="post">
		
 
            
            
            
			
			<tr>
				<td width="160px">Doctor's ID：</td>
				<td><input type="text" name="id" /></td>
			</tr>
                        <tr>Ratings</tr>
                        <select name="ratings" size="1">
              
                        <option>5</option>
                        <option>4</option>
                        <option>3</option>
                        <option>2</option>
                        <option>1</option>
                        </select>
                        <tr>
				<td align="center" colspan="2"><input type="submit" class="btn btn-info"
					value="Set"></td>
			</tr>
                        
            </form>
</div>
</body>
</html>

