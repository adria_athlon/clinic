<%-- 
    Document   : index
    Created on : Jan 16, 2020, 10:49:38 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login Page</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
    
  <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
  <ul class="navbar-nav">
    <a class="navbar-brand" href="index.jsp">Main Menu</a>  
    <li class="nav-item">
      <a class="nav-link" href="./PatientLogin.jsp">Patient Login</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="./StaffLogin.jsp">Staff Login</a>
    </li>    
  </ul>

   </nav>

<div class="jumbotron text-left" style="background-image: url('./img/banner-bg.jpg'); 
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    height: 100vh;
    width: 100vw;">   
  
    <h1 style="font-size:50px;color:blue;"><b>Welcome to Somewhere Clinic</b></h1>
    <form action="LoginServlet" method="post" name="form1">
        <BR><b style="color:grey;">Select your job function：</b><select name="UserSort" size="1">
              
              <option>Administrator</option>
              <option>Secretary</option>
              <option>Doctor</option>
            </select>
        </BR>
        UserID：<BR><input name="username" type="text" maxlength="20"></BR>
        Password：<BR><input name="pwd" type="password" size="20" maxlength="20"></BR>
        <br></br>
        <input type="submit" class="btn btn-info" value="Submit">
        
</form>
</div>
</body>
</html>

