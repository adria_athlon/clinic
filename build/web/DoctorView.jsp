<%-- 
    Document   : DoctorView
    Created on : Jan 16, 2020, 10:48:54 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="DoctorView.jsp">Doctor Main Menu</a> 
    
    </ul>
    <div class="navbar-nav navbar-right">
        <a href="./index.jsp">Logout</a>
    </div>
  </nav>
<div class="container">
    <div class="row align-items-start">
    <div class="col-6">
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Prescribe medicine</h5>
        <p class="card-text">Prescribe medicine and dosages.</p>
        <a href="./doctor/PrescribeMedicinesAndDosages.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Order medicine</h5>
        <p class="card-text">Order medicine stock</p>
        <a href="./doctor/OrderMedicine.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>    
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Review Patient Clinic Records</h5>
        <p class="card-text">Review Patient clinic record history</p>
        <a href="./doctor/ViewPatientHistory.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">View scheduled appointment</h5>
        <p class="card-text">Review your forthcoming appointment</p>
        <a href="./doctor/ViewAppointments.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Create appointment</h5>
        <p class="card-text">Create forthcoming appointment for patient</p>
        <a href="./doctor/ManageAppointment.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
      
    
</div>
    
</body>
</html>

