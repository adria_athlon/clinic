<%-- 
    Document   : PatientView
    Created on : Jan 16, 2020, 10:49:05 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


</head>

<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="PatientView.jsp">Patient Main Menu</a> 
    
    </ul>
    <div class="navbar-nav navbar-right">
        <a href="./index.jsp">Logout</a>
    </div>
  </nav>
<div class="container">
    <div class="row align-items-start">
    <div class="col-6">
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Appointment Request</h5>
        <p class="card-text">Schedule new appointment upon doctor's availability.</p>
        <a href="./patient/RequestAppointment.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Review Clinic Records</h5>
        <p class="card-text">Review your clinic record history</p>
        <a href="./patient/ViewHistory.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">View scheduled appointment</h5>
        <p class="card-text">Review your forthcoming appointment</p>
        <a href="./patient/ViewAppointment.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    
    <div class="col-6">
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">View your prescription</h5>
        <p class="card-text">View your prescription and collect if needed</p>
        <a href="./patient/ViewPrescription.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>    
    <div class="col-6">
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">View Doctor Ratings</h5>
        <p class="card-text">Review our doctors</p>
        <a href="./patient/ViewDoctorsRatings.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Set Doctor Ratings</h5>
        <p class="card-text">Rate our doctors</p>
        <a href="./patient/SetDoctorsRatings.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
        
    <div class="col-6">
    <div class="card text-white bg-danger ">
      <div class="card-body">
        <h5 class="card-title">Request Account Termination</h5>
        <p class="card-text">Send request to terminate your current account.</p>
        <a href="./patient/TerminateAccount.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    
</div>
</div>       
</body>
</body>
</html>

