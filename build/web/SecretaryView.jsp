<%-- 
    Document   : SecretaryView
    Created on : Jan 16, 2020, 10:49:20 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="SecretaryView.jsp">Secretary Main Menu</a> 
    
    </ul>
    <div class="navbar-nav navbar-right">
        <a href="./index.jsp">Logout</a>
    </div>
  </nav>
<div class="container">
    <div class="row align-items-start">
    <div class="col-6">
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Create Appointment</h5>
        <p class="card-text">Create an appointment between a patient and a doctor.</p>
        <a href="./secretary/ManageAppointment.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Order Medicine</h5>
        <p class="card-text">Order Medicine Stock</p>
        <a href="./secretary/OrderMedicine.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>     
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Dispatch Medicine</h5>
        <p class="card-text">Review patient prescription and dispatch medicine</p>
        <a href="./secretary/ViewPrescription.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Patient Approval</h5>
        <p class="card-text">Approve new patient account.</p>
        <a href="./secretary/ApprovePatient.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
    <div class="col-6">    
    <div class="card bg-light ">
      <div class="card-body">
        <h5 class="card-title">Patient Termination</h5>
        <p class="card-text">Approve to terminate patient account.</p>
        <a href="./secretary/RemovePatients.jsp" class="btn btn-primary">Start here</a>
      </div>
    </div>
    </div>
          
    
</div>
    
</body>
</html>

