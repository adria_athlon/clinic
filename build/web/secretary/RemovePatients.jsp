<%-- 
    Document   : RemovePatients
    Created on : Jan 16, 2020, 10:57:20 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


<title>Remove Patients</title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="SecretaryView.jsp">Secretary Main Menu</a> 
    
    </ul>
    
  </nav>
<div class="container">
	<form name="removeR_from" action="../SecretaryRemovePatientServlet"
		method="post" target="_self">
		<table align="center">
			<tr>
				<td align="center" colspan="2">
					<h3>Patient Termination</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td width="160px">Patient's ID：</td>
				<td colspan="2"><select name="patientID" size="1">
						<option>P1001</option>
						<option>P1002</option>
						<option>P1003</option>
				</select></td>
			</tr>
			<tr>
				<td width="160px">Patient's password：</td>
				<td><input type="text" name="patientPwd" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit" class="btn btn-info"
					value="remove"></td>
			</tr>
		</table>
	</form>
</div>
</body>
</html>

