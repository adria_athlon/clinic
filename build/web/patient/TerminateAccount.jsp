<%-- 
    Document   : TerminateAccount
    Created on : Jan 16, 2020, 10:58:51 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<title>Terminate Account</title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="PatientView.jsp">Patient Main Menu</a> 
    
    </ul>
    
  </nav>
<div class="container">
   <form name="terminate_form" action="../PatientTerminateRequestServlet" method="post" >
		<table align="center" width="400" cellspacing="5px">
			<tr align="center">
				<td align="center" colspan="2">
					<h3>Request Account Termination</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td  width="160px">Patient's Unique ID：</td>
				<td><input type="text" name="id"/></td>
			</tr>			
			<tr>
				<td  width="160px">Patient's Password：</td>
				<td><input type="text" name="pwd"/></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<input type="submit" class="btn btn-info" value="Terminate">
				</td>
			</tr>
	     </table>
    </form>
</div>
</body>
</html>

