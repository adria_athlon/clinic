<%-- 
    Document   : ManageAppointment
    Created on : Jan 16, 2020, 10:56:52 AM
    Author     : Alan Chan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<title>Manage Appointment</title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

  <!-- Links -->
    <ul class="navbar-nav">
    <a class="navbar-brand" href="DoctorView.jsp">Doctor Main Menu</a> 
    
    </ul>
    
  </nav>
<div class="container">
	<form name="manage_appointment_from"
		action="../DoctorManageAppointment" method="post" target="_self">
		<table align="center">
			<tr>
				<td align="center" colspan="2">
					<h3>Create Appointment</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td width="260px">Patient ID：</td>
				<td><input type="text" name="patientID" /></td>
			</tr>
			<tr>
				<td width="260px">Doctor ID：</td>
				<td><input type="text" name="doctorID" /></td>
			</tr>
			<tr>
				<td width="260px">Start Time：</td>
				<td><input type="text" name="startTime" /></td>
			</tr>
			<tr>
				<td width="260px">End Time：</td>
				<td><input type="text" name="endTime" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit" class="btn btn-info"
					value="Create"></td>
			</tr>
		</table>
	</form>
</div>
</body>
</html>

