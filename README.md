This is a program written in Java. This project could be downloaded by issue below command

git clone https://adria_athlon@bitbucket.org/adria_athlon/clinic.git

required libraries 
javax.servlet-3.0.jar
GlassFishServer
Java 1.8

The directory structure should be as below.

+---build
|   +---empty
|   +---generated-sources
|   |   \---ap-source-output
|   \---web
|       +---administrator
|       +---doctor
|       +---img
|       +---META-INF
|       +---patient
|       +---secretary
|       \---WEB-INF
|           +---classes
|           |   +---ControllerServlet
|           |   +---DAL
|           |   +---MessageObserver
|           |   \---model
|           \---lib
+---db
+---dist
+---lib
+---nbproject
|   \---private
+---src
|   +---conf
|   \---java
|       +---ControllerServlet
|       +---DAL
|       +---MessageObserver
|       \---model
+---test
\---web
    +---administrator
    +---doctor
    +---img
    +---patient
    +---secretary
    \---WEB-INF
        \---lib

After open the project in netbeans with Web server depolyed, the project should be able to start and the front page is index.jsp.

There are several user accounts set by default:-
   Role       : User  / Password
Administrator : A0001 / P@ssw0rd
Secretary     : S0001 / P@ssw0rd
Doctor        : D0001 / P@ssw0rd
Patient       : P0001 / P@ssw0rd
