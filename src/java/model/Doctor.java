/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * this is the doctor user model
 * @author Alan Chan
 */
public class Doctor extends ClinicUser{
	
	private String role;
	private int ratings;
        private String adminComment;

    /**
     * get administrator comment
     * @return
     */
    public String getAdminComment() {
        return adminComment;
    }

    /**
     * set administrator comment
     * @param adminComment
     */
    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }
        
    /**
     * get doctor ratings
     * @return
     */
    public int getRatings() {
        return ratings;
    }

    /**
     * set doctor ratings
     * @param ratings
     */
    public void setRatings(int ratings) {
        this.ratings = ratings;
    }
	private Appointment appointment;
	private String address;

    /**
     *
     * @param id
     * @param pwd
     */
    public Doctor(String id, String pwd) {
		super(id, pwd);
		role = "doctor";
	}

    /**
     *
     * @return
     */
    public String getRole() {
		return role;
	}

    /**
     * get doctor appointment
     * @return
     */
    public Appointment getAppointment() {
		return appointment;
	}

    /**
     *
     * @param appointment
     */
    public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

    /**
     * get doctor address
     * @return
     */
    public String getAddress() {
		return address;
	}

    /**
     * set doctor address
     * @param address
     */
    public void setAddress(String address) {
		this.address = address;
	}

    /**
     *
     * @param appointment
     */
    @Override
	public void update(Appointment appointment) {
		if(getId().equals(appointment.getDoctorid()))
			this.appointment = appointment;
	}
}
