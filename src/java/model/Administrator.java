/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * This is the admin user model
 * @author Alan Chan
 */
public class Administrator extends ClinicUser {
	private String role;

    /**
     *
     * @param id
     * @param pwd
     */
    public Administrator(String id, String pwd) {
		super(id, pwd);
		role = "administrator";
	}

    /**
     *
     * @return
     */
    public String getRole() {
		return role;
	}
}

