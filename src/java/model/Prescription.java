/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * this is the prescription mode
 * @author Alan Chan
 */
public class Prescription {
	private Medicine medicine;
	private int quantity;
	private String dosage;
	private String doctorID;
	private String patientID;
        private String note;

    
    /**
     * prescription only without signiture
     * @param medicine selected medicine
     * @param quantity selected qty
     * @param dosage selected dosage
     */
    public Prescription(Medicine medicine, int quantity, String dosage) {
		super();
		this.medicine = medicine;
		this.quantity = quantity;
		this.dosage = dosage;
	}

    /**
     * doctor prescription
     * @param medicine
     * @param quantity
     * @param dosage
     * @param doctorID
     * @param patientID
     * @param note
     */
    public Prescription(Medicine medicine, int quantity, String dosage, String doctorID, String patientID, String note) {
		super();
		this.medicine = medicine;
		this.quantity = quantity;
		this.dosage = dosage;
		this.doctorID = doctorID;
		this.patientID = patientID;
                this.note=note;
	}

    /**
     *
     * @return
     */
    public int getQuantity() {
		return quantity;
	}

    /**
     *
     * @param quantity
     */
    public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

    /**
     *
     * @return
     */
    public String getDosage() {
		return dosage;
	}

    /**
     *
     * @param dosage
     */
    public void setDosage(String dosage) {
		this.dosage = dosage;
	}

    /**
     *
     * @return
     */
    public String getDoctorID() {
		return doctorID;
	}

    /**
     *
     * @param doctorID
     */
    public void setDoctorID(String doctorID) {
		this.doctorID = doctorID;
	}

    /**
     *
     * @return
     */
    public String getPatientID() {
		return patientID;
	}

    /**
     *
     * @param patientID
     */
    public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
    
    /**
     *
     * @return
     */
    public Medicine getMedicine() {
        return medicine;
    }

    /**
     *
     * @param medicine
     */
    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    /**
     *
     * @return
     */
    public String getNote() {
        return note;
    }

    /**
     *
     * @param note
     */
    public void setNote(String note) {
        this.note = note;
    }
	
}

