/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import MessageObserver.Observer;

/**
 * this is the user model class
 * @author Alan Chan
 */
public class ClinicUser implements Observer {
	private String id;
	private String pwd;
        private String name;

    /**
     * get user name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * set user name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @param id user id
     * @param pwd password
     */
    public ClinicUser(String id,String pwd) {
		this.id = id;
		this.pwd = pwd;
	}

    /**
     * get user id
     * @return
     */
    public String getId() {
		return id;
	}

    /**
     * set user id
     * @param id
     */
    public void setId(String id) {
		this.id = id;
	}

    /**
     * get user password
     * @return
     */
    public String getPwd() {
		return pwd;
	}

    /**
     * set user password
     * @param pwd
     */
    public void setPwd(String pwd) {
		this.pwd = pwd;
	}

    /**
     * create user appointment
     * @param appointment
     */
    @Override
	public void update(Appointment appointment) {
		System.out.println("New Appointment: " + appointment);
	}
	
}

