/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * This is the medicine model
 * @author Alan Chan
 */
public class Medicine {
    private String name;
    private int stock;
    
    /**
     *
     * @param name medicine name
     * @param stock medicine stock in clinic
     */
    public Medicine (String name,int stock){
        this.name=name;
        this.stock=stock;
    }

    public String getName() {
        return name;
    }

    
    /**
     *
     * @return
     */
    public String getName(Medicine medicine) {
        return medicine.name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public int getStock() {
        return stock;
    }

    /**
     *
     * @param stock
     */
    public void setStock(int stock) {
        this.stock = stock;
    }
}
