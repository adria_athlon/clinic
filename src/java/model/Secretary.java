/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * this is the user model for secretary
 * @author Alan Chan
 */
public class Secretary extends ClinicUser{
	
	private String role;

    /**
     *
     * @param id
     * @param pwd
     */
    public Secretary(String id, String pwd) {
		super(id, pwd);
		role = "secretary";
	}

    /**
     *
     * @return
     */
    public String getRole() {
		return role;
	}
}

