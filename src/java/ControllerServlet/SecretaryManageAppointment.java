/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

/**
 *
 * @author Alan Chan
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.jws.Oneway;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAL.DAOFactory;
import DAL.dataOperator;
import DAL.DataInit;
import MessageObserver.MessageServer;
import MessageObserver.Observer;
import model.Appointment;
import model.Doctor;
import model.Patient;

/**
 * this servlet used for secretary to manage appointment
 * @author Alan Chan
 */
@WebServlet(name = "SecretaryManageAppointment",urlPatterns ="/SecretaryManageAppointment")
public class SecretaryManageAppointment extends HttpServlet {
	private static final long serialVersionUID = 1L;   
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}
	
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String patientID = request.getParameter("patientID");
		String doctorID = request.getParameter("doctorID");
		String startTime = request.getParameter("startTime");	
		String endTime = request.getParameter("endTime");
		
		Appointment appointment = new Appointment(patientID,doctorID,startTime, endTime);
		
		DataInit.appointmentsList.add(appointment);
		
		MessageServer messageServer = new MessageServer();
		Observer patientObserver = null;
		Observer doctorObserver = null;
		for(Patient patient : DataInit.patientList) {
			if(patient.getId().equals(patientID)) {
				patientObserver = patient;
			}
		}
		for(Doctor doctor : DataInit.doctorList) {
			if(doctor.getId().equals(doctorID)) {
				doctorObserver = doctor;
			}
		}
		messageServer.registerObserver(patientObserver);
		messageServer.registerObserver(doctorObserver);
		messageServer.setMessageAndNotifyAll(appointment);

		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str='Created new appointment';");
		out.println("alert(str);");
		out.println("window.location.href='SecretaryView.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();
	}
}

