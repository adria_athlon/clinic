/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAL.DAOFactory;
import DAL.dataOperator;
import DAL.DataInit;
import model.Administrator;
import model.Doctor;
import model.Patient;
import model.Secretary;

/**
 * this servlet used for admin to create account
 * @author Alan Chan
 */
@WebServlet(name = "AdminCreateAccountServlet", urlPatterns = "/AdminCreateAccountServlet")
public class CreateAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String accountID = request.getParameter("id");
		String accountPwd = request.getParameter("pwd");
		String accountType = request.getParameter("UserSort");
		if(accountType.equals("Patient")) {
			DataInit.patientList.add(new Patient(accountID, accountPwd));
		}else if(accountType.equals("Doctor")) {
			DataInit.doctorList.add(new Doctor(accountID, accountPwd));
		}else if(accountType.equals("Administrator")) {
			DataInit.administratorList.add(new Administrator(accountID, accountPwd));
		}else if(accountType.equals("Secretary")) {
			DataInit.secretaryList.add(new Secretary(accountID, accountPwd));
		}
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str=' create account Completed';");
		out.println("alert(str);");
		out.println("window.location.href='AdministratorView.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();	
	}
}
