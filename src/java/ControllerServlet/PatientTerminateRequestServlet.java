/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

/**
 *
 * @author Alan Chan
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAL.DAOFactory;
import DAL.dataOperator;

/**
 *
 * this servlet used for patients to request account termination
 * @author Alan Chan
 */
@WebServlet(name = "PatientTerminateRequestServlet",urlPatterns ="/PatientTerminateRequestServlet")
public class PatientTerminateRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;	   
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String patientID = request.getParameter("id");
		String patientPwd = request.getParameter("pwd");
		
		dataOperator.removePatient(patientID, patientPwd);
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str=' Terminate Patient Completed';");
		out.println("alert(str);");
		out.println("window.location.href='PatientView.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();	
	}
}
