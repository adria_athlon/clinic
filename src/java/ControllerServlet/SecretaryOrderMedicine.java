/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

import DAL.DAOFactory;
import DAL.DataInit;
import DAL.dataOperator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Medicine;
import model.Patient;
import model.Prescription;

/**
 * this servlet used for secretary to order medicine
 * @author Alan Chan
 */
@WebServlet(name = "SecretaryOrderMedicine", urlPatterns = {"/SecretaryOrderMedicine"})
public class SecretaryOrderMedicine extends HttpServlet {
	private static final long serialVersionUID = 1L;   
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}
    
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
                response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
                
		String medicine = request.getParameter("medicine");
		int quantity = Integer.valueOf(request.getParameter("quantity"));
		
		
                
                for(Medicine medlist : DataInit.medicineList) {
			if(medlist.getName().equals(medicine)) {
                            int currentStock=medlist.getStock();
                            medlist.setStock(currentStock+quantity);
       			}
		}
                
               
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str='Order Medicines Completed';");
		out.println("alert(str);");
		out.println("window.location.href='SecretaryView.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();
        
        
    }

    

}
