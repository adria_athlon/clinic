/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

/**
 *
 * @author Alan Chan
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAL.DAOFactory;
import DAL.dataOperator;
import DAL.DataInit;
import model.Appointment;
import model.Medicine;
import model.Patient;
import model.Prescription;

/**
 * this servlet used for doctors to prescribe medicines
 * @author Alan Chan
 */
@WebServlet(name = "DoctorPrescribeMedicinesServlet",urlPatterns ="/DoctorPrescribeMedicinesServlet")
public class DoctorPrescribeMedicinesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;   
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}
	
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String patientID = request.getParameter("patientID");
		String medicine = request.getParameter("medicine");
		int quantity = Integer.valueOf(request.getParameter("quantity"));
		String dosage = request.getParameter("dosage");
		String doctorID = request.getParameter("doctorID");
                String note = request.getParameter("note");
		
                Medicine selection = new Medicine(medicine,quantity);
                for(Medicine medlist : DataInit.medicineList) {
			if(medlist.getName().equals(medicine)) {
                            int currentStock=medlist.getStock();
                            medlist.setStock(currentStock-quantity);
       			}
		}
                
               	Prescription prescription = new Prescription(selection,quantity,dosage,doctorID,patientID,note);		
		DataInit.prescriptionsList.add(prescription);
                
		for(Patient patient : DataInit.patientList) {
			if(patient.getId().equals(patientID)) {
				patient.setPrescription(prescription);
				patient.setHistory("Got the Prescription, the Medicine is : " + selection);
			}
		}
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str='Prescribe Medicines Done';");
		out.println("alert(str);");
		out.println("window.location.href='DoctorView.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();
	}
}
