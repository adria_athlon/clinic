/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

/**
 *
 * @author Alan Chan
 */
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAL.DAOFactory;
import DAL.dataOperator;
import DAL.DataInit;
import model.Appointment;
import model.Patient;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * this servlet used for patients to view history
 * @author Alan Chan
 */
@WebServlet(name = "PatientViewHistoryServlet", urlPatterns = "/PatientViewHistoryServlet")
public class PatientViewHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private dataOperator dataOperator;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String patientID = request.getParameter("id");
		
		List<Patient> p = new ArrayList<>();

		for (Patient patient : DataInit.patientList) {
			if(patientID.equals(patient.getId()))
				p.add(patient);
		}
		request.setAttribute("patientList", p);
		request.getRequestDispatcher("/patient/ViewHistory.jsp").forward(request, response);
	}
}

