/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAL.DAOFactory;
import DAL.dataOperator;
import DAL.DataInit;
import model.Patient;

/**
 * this servlet used for patients to request account creation
 * @author Alan Chan
 */
@WebServlet(name = "PatientCreateAccount", urlPatterns = "/PatientCreateAccount") 
public class PatientCreateAccount extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}
	
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String patientID = request.getParameter("id");
		String patientPwd = request.getParameter("pwd");
		int patientAge = Integer.valueOf(request.getParameter("age"));
		String patientAddress = request.getParameter("address");
		String patientGender = request.getParameter("gender");
		
		DataInit.patientList.add(new Patient(patientID, patientPwd, patientAge, patientGender, patientAddress));
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str='Information collected, please wait for your account approval.';");
		out.println("alert(str);");
		out.println("window.location.href='index.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();	
                
	}
}
