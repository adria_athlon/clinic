/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

/**
 *
 * @author Alan Chan
 */

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAL.DAOFactory;
import DAL.dataOperator;
import DAL.DataInit;
import model.Appointment;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * this servlet used for doctors to view appointments
 * @author Alan Chan
 */
@WebServlet(name = "DoctorViewAppointmentServlet", urlPatterns = "/DoctorViewAppointmentServlet")
public class DoctorViewAppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private dataOperator dataOperator;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String doctorID = request.getParameter("id");
		
		List<Appointment> appointment = new ArrayList<>();
		for (Appointment app : DataInit.appointmentsList) {
			if(doctorID.equals(app.getDoctorid())) {
				appointment.add(app);
			}
		}
		request.setAttribute("list", appointment);
		request.getRequestDispatcher("/doctor/ViewAppointments.jsp").forward(request, response);
	}
}
