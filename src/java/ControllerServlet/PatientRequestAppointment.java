/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

/**
 *
 * @author Alan Chan
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAL.DAOFactory;
import DAL.dataOperator;
import DAL.DataInit;
import model.Appointment;

/**
 * this servlet used for patients to request new appointment
 * @author Alan Chan
 */
@WebServlet(name = "PatientRequestAppointment",urlPatterns ="/PatientRequestAppointment")
public class PatientRequestAppointment extends HttpServlet {
	private static final long serialVersionUID = 1L;   
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}
	
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String patientID = request.getParameter("patientID");
		String doctorID = request.getParameter("doctorID");
		String startTime = request.getParameter("startTime");	
		String endTime = request.getParameter("endTime");
		
		DataInit.appointmentsList.add(new Appointment(patientID,doctorID,startTime, endTime));
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str='Request Appointment Completed ！';");
		out.println("alert(str);");
		out.println("window.location.href='PatientView.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();
	}
}

