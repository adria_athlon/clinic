/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

import DAL.DAOFactory;
import DAL.DataInit;
import DAL.dataOperator;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Doctor;


/**
 * this servlet used for patients to view doctor ratings
 * @author Alan Chan
 */
@WebServlet(name = "ViewDoctorsRatings", urlPatterns = {"/ViewDoctorsRatings"})
public class PatientViewDoctorRatings extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String doctorID = request.getParameter("id");
		String name="";
		int ratings=0;

		for (Doctor doctor : DataInit.doctorList) {
			if(doctorID.equals(doctor.getId())){
                                name=doctor.getName();
				ratings=doctor.getRatings();
                        }
		}
                
                request.setAttribute("name", name);
		request.setAttribute("ratings", ratings);
                               
		request.getRequestDispatcher("/patient/ViewDoctorsRatings.jsp").forward(request, response);
	}
}

    