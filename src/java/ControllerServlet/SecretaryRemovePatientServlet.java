/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerServlet;

/**
 *
 * @author Alan Chan
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAL.DAOFactory;
import DAL.dataOperator;

/**
 *
 * this servlet used for secretary to remove patient account
 * @author Alan Chan
 */
@WebServlet(name = "SecretaryRemovePatientServlet",urlPatterns ="/SecretaryRemovePatientServlet")
public class SecretaryRemovePatientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;	   
	private dataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DAOFactory.getSingleDataOperator();
	}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String patientID = request.getParameter("patientID");
		String patientPwd = request.getParameter("patientPwd");
		dataOperator.removePatient(patientID, patientPwd); 
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str=' Patient Removed ';");
		out.println("alert(str);");
		out.println("window.location.href='SecretaryView.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();	
	}
}

