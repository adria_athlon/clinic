/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageObserver;

/**
 * this is the observer model
 * @author Alan Chan
 */
import model.Appointment;

/**
 *
 * @author Alan Chan
 */
public interface Observer  {

    /**
     *
     * @param appointment
     */
    public void update(Appointment appointment);
}

