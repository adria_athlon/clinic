/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageObserver;

/**
 * this is the subscription model
 * @author Alan Chan
 */
public interface Observerable {

    /**
     *
     * @param observer
     */
    public void registerObserver(Observer observer);

    /**
     *
     * @param observer
     */
    public void removeObserver(Observer observer);
    
    /**
     *
     */
    public void notifyObserver();
}

