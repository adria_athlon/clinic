/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageObserver;

/**
 * this is the message server
 * @author Alan Chan
 */
import java.util.ArrayList;
import java.util.List;

import model.Appointment;

/**
 *
 * @author Alan Chan
 */
public class MessageServer implements Observerable {
	
	private List<Observer> observersList;
	private Appointment appointment;
	
    /**
     *
     */
    public MessageServer() {
		observersList = new ArrayList<>();
	}

    /**
     * subscribe alert
     * @param observer
     */
    @Override
	public void registerObserver(Observer observer) {
		observersList.add(observer);
	}

    /**
     * remove alert
     * @param observer
     */
    @Override
	public void removeObserver(Observer observer) {
		if(!observersList.isEmpty()) {
			observersList.remove(observer);
		}
	}

    /**
     *
     */
    @Override
	public void notifyObserver() {
		for(Observer observer : observersList) {
			observer.update(appointment);
		}
	}

    /**
     *
     * @param appointment
     */
    public void setMessageAndNotifyAll(Appointment appointment) {
		this.appointment = appointment;
		notifyObserver();
	}

}

