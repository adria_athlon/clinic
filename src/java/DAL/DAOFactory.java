/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

/*
 * this is the DAO factory
 * @author Alan Chan
 */
public class DAOFactory {
   private volatile static dataOperator dataOperator;

	private DAOFactory() {
	}

    /**
     * 
     * @return
     */
    public static dataOperator getSingleDataOperator() {
		if (dataOperator == null) {
			synchronized (DAOFactory.class) {
				if (dataOperator == null) {
					dataOperator = new dataOperator();
				}
			}
		}
		return dataOperator;
	}
}

