/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

/**
 * This model creates dummy data
 * @author Alan Chan
 */
import java.util.LinkedList;
import java.util.List;


import model.Administrator;
import model.Appointment;
import model.Doctor;
import model.Medicine;
import model.Patient;
import model.Prescription;
import model.Secretary;

/**
 *
 * @author Alan Chan
 */
public class DataInit {

    /**
     *Create new list of patient
     */
    public static List<Patient> patientList = new LinkedList<>();

    /**
     *Create new list of doctor
     */
    public static List<Doctor> doctorList = new LinkedList<>();

    /**
     *Create new list of secretary
     */
    public static List<Secretary> secretaryList = new LinkedList<>();

    /**
     *Create new list of administrator
     */
    public static List<Administrator> administratorList = new LinkedList<>();

    /**
     *Create new list of appointment
     */
    public static List<Appointment> appointmentsList = new LinkedList<>();

    /**
     *Create new list of prescriptions
     */
    public static List<Prescription> prescriptionsList = new LinkedList<>();

    /**
     *Create new list of medicine
     */
    public static List<Medicine> medicineList = new LinkedList<>();
     
    /**
     *Apply dummy data to the lists
     */
    public void init() {
		
		Patient alpha = new Patient("P0001","P@ssw0rd");
                Patient beta = new Patient("P0002", "P@ssw0rd");
		Patient grama = new Patient("P0003", "P@ssw0rd");
                
                Doctor choi = new Doctor("D0001", "P@ssw0rd");
                Doctor cheung = new Doctor("D0002","P@ssw0rd");
                Doctor ho = new Doctor("D0003","P@ssw0rd");
                
                choi.setName("Choi");
                cheung.setName("Cheung");
                ho.setName("Ho");
                
                
                Administrator admin = new Administrator("A0001", "P@ssw0rd");
		Secretary secretary = new Secretary("S0001", "P@ssw0rd");
		                
		alpha.setAddress("Somewhere in HK");
		alpha.setGender("Male");
		alpha.setAge(25);
		alpha.setHistory("SARS");
				
		choi.setAddress("Victoria Peak");
                
                choi.setRatings(5);
		cheung.setRatings(4);
		ho.setRatings(3);
                
                Appointment appointment = new Appointment("P0001","D0001","2020-1-1","2020-1-2");
                
                Medicine amoxcillin = new Medicine("amoxicillin",100);
		Medicine levothyroxine = new Medicine("levothyroxine",100);
                Medicine rosuvastatin = new Medicine("rosuvastatin",100);
                Medicine albuterol = new Medicine("albuterol",100);
                Medicine esomeprazole = new Medicine("esomeprazole",100);
                Medicine fluticasone = new Medicine("fluticasone",100);
                Medicine lisdexamfetamine = new Medicine("lisdexamfetamine",100);
                Medicine pregabalin = new Medicine("pregabalin",100);
                Medicine sitagliptin = new Medicine("sitagliptin",100);
                Medicine tiotropium = new Medicine("tiotropium",100);
                         
               	int quantity = 16;
		String dosage = "4 per day every 4 hours";
                
		Prescription prescription = new Prescription(amoxcillin, quantity, dosage);
		prescription.setDoctorID("D0001");
		prescription.setPatientID("P0001");
                prescription.setNote("No Cure");
		alpha.setPrescription(prescription);
                
		alpha.setAppointment(appointment);
		choi.setAppointment(appointment);
				
		DataInit.patientList.add(alpha);
		DataInit.patientList.add(beta);
		DataInit.patientList.add(grama);
		DataInit.doctorList.add(choi);
		DataInit.doctorList.add(cheung);
		DataInit.doctorList.add(ho);
		DataInit.appointmentsList.add(appointment);
		DataInit.prescriptionsList.add(prescription);
		DataInit.administratorList.add(admin);
		DataInit.secretaryList.add(secretary);
	}
}


